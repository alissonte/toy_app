class User < ActiveRecord::Base
  validates_presence_of :name
  validates_presence_of :email
  validates_uniqueness_of :email
  validates_length_of :name, :maximum => 50
  has_many :microposts
end
